//
//  APIManager.swift
//  ATeamsTestTask
//
//  Created by MacBookPro on 04/10/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import Foundation

enum APIResult {
    case Success([String: Any])
    case Failure(Error)
}

class APIManager {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    
    func JSONTaskWith(request: URLRequest, completionHandler: @escaping ([String: AnyObject]?, HTTPURLResponse?, Error?) -> Void) -> URLSessionDataTask {
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            guard let HTTPResponse = response as? HTTPURLResponse else {
                let userInfo = [
                    NSLocalizedDescriptionKey: NSLocalizedString("Missing HTTP Response", comment: "")
                ]
                let error = NSError(domain: SWINetworkingErrorDomain, code: MissingHTTPResponseError, userInfo: userInfo)
                completionHandler(nil, nil, error)
                return
            }
            
            if data == nil {
                if let error = error {
                    completionHandler(nil, nil, error)
                }
            } else {
                switch HTTPResponse.statusCode {
                case 200: do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                    completionHandler(json, HTTPResponse, nil)
                } catch let error as NSError {
                    completionHandler(nil, HTTPResponse, error)
                    }
                default:
                    print("We have got http status \(HTTPResponse.statusCode)")
                }
                
            }
        }
        return dataTask
    }
    
    func fetchData(dataType subdomain: Subdomain, path: String = "/", completionHandler: @escaping (APIResult) -> Void) {
        let baseURLString = "http://\(subdomain.rawValue).jsontest.com"
        let baseURL = URL(string: baseURLString)
        let url: URL?
        if subdomain == .validate {
            var urlComponents = URLComponents(string: baseURLString)!
            urlComponents.queryItems = [
                URLQueryItem(name: "json", value: path),
            ]
            url = urlComponents.url
        } else {
            if path.trimmingCharacters(in: .whitespaces) == "" {
                url = URL(string: "/", relativeTo: baseURL)
            } else {
                url = URL(string: path, relativeTo: baseURL)
            }
        }
        
        if let url = url {
            let request = URLRequest(url: url)
            let dataTask = JSONTaskWith(request: request) {(json, response, error) in
                
                DispatchQueue.main.async {
                    guard let json = json else {
                        if let error = error {
                            completionHandler(.Failure(error))
                        }
                        return
                    }
                    completionHandler(.Success(json))
                }
                
            }
            dataTask.resume()
        }

    }
    
}




