//
//  ViewController.swift
//  ATeamsTestTask
//
//  Created by MacBookPro on 04/10/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class ItemsViewController: UITableViewController {
    
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var headersTextView: UITextView!
    @IBOutlet weak var validateTextView: UITextView!
    @IBOutlet weak var echoTextView: UITextView!
    @IBOutlet weak var echoButton: UIButton!
    @IBOutlet weak var validateButton: UIButton!
    
    let apiManager = APIManager()
    
    var values: [String: [String: Any]] = [:]
    
    var clearEchoResult = false
    var lastEchoParams = "/insert-key-here/insert-value-here/key/value"
    
    var clearValidateResult = false
    var lastJSONToValidate = "{\"key\":\"value\"}"
    
    @IBAction func echoButtonTapped(_ sender: UIButton) {
        if clearEchoResult {
            echoButton.titleLabel?.text = "Submit"
            echoTextView.text = lastEchoParams
            clearEchoResult = false
        } else {
            let path = echoTextView.text
            getJSONResponseInDict(apiManager: apiManager, dataType: .echo, path: path ?? "/")
            echoTextView.resignFirstResponder()
        }

    }
    @IBAction func validateButtonTapped(_ sender: UIButton) {
        if clearValidateResult {
            validateButton.titleLabel?.text = "Submit"
            validateTextView.text = lastJSONToValidate
            clearValidateResult = false
        } else {
            let path = validateTextView.text!
            getJSONResponseInDict(apiManager: apiManager, dataType: .validate, path: path)
            validateTextView.resignFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsetsMake(0, 0, self.tabBarController!.tabBar.frame.height, 0);
        //Where tableview is the IBOutlet for your storyboard tableview.
        self.tableView.contentInset = adjustForTabbarInsets;
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets;
        
        getJSONResponseInDict(apiManager: apiManager, dataType: .ip)
        getJSONResponseInDict(apiManager: apiManager, dataType: .date)
        getJSONResponseInDict(apiManager: apiManager, dataType: .headers)
        
    }
    
    override func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red:0.70, green:0.62, blue:0.86, alpha:1.0)
    }
    
    func updateUI(dataType: Subdomain) {
        switch dataType {
        case .ip:
            if let ip = values["ip"] {
                ipLabel.text = String(describing: ip["ip"]!)
            }
        case .date:
            if let date = values["date"] {
                if let mse = date["milliseconds_since_epoch"] as? Double {
                    let date = NSDate(timeIntervalSince1970: (mse / 1000))
                    
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd.MM.YYYY HH:MM"
                    
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    dateTimeLabel.text = dateString
                }
                
            }
        case .headers:
            if let headers = values["headers"] {
                headersTextView.text = prettyfyDict(dic: headers)!
                headersTextView.isScrollEnabled  = true
            }
        case .echo:
            if let echoData = values["echo"] {
                echoTextView.text = prettyfyDict(dic: echoData)
                clearEchoResult = true
                echoButton.titleLabel?.text = "Return"
            }
        case .validate:
            if let validationData = values["validate"] {
                validateTextView.text = prettyfyDict(dic: validationData)
                clearValidateResult = true
                validateButton.titleLabel?.text = "Return"
            }
        }
        
    }
    
    func getJSONResponseInDict(apiManager: APIManager, dataType subdomain: Subdomain, path: String = "/") {
        apiManager.fetchData(dataType: subdomain, path: path) {
            [weak self] (result) in
            switch result {
            case .Success(let currentData):
                print(currentData)
                self?.values[subdomain.rawValue] = currentData
                switch subdomain {
                case .echo:
                    self?.lastEchoParams = path
                case .validate:
                    self?.lastJSONToValidate = path
                default: break
                }
                
                DispatchQueue.main.async {
                    self?.updateUI(dataType: subdomain)
                }
                
            case .Failure(let error as NSError):
                let alertController = UIAlertController(title: "Unable to get data", message: "\(error.localizedDescription)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self?.present(alertController, animated: true, completion: nil)
            default: break
            }
        }
    }
    
    func prettyfyDict(dic: Any) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            return jsonString
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
}

