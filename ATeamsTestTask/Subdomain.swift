//
//  Subdomain.swift
//  ATeamsTestTask
//
//  Created by MacBookPro on 04/10/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import Foundation

enum Subdomain: String {
    case ip = "ip"
    case headers = "headers"
    case date = "date"
    case echo = "echo"
    case validate = "validate"
}
