//
//  ErrorManager.swift
//  ATeamsTestTask
//
//  Created by MacBookPro on 04/10/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import Foundation

public let SWINetworkingErrorDomain = "com.maxibello.ATeamsTestTask.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
